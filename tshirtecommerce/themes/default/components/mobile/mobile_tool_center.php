<?php 
$products = $GLOBALS['products'];
$addons = $GLOBALS['addons'];
$settings = $GLOBALS['settings'];
?>
<div class="col-center align-center">
	<!-- design area -->
	<div id="design-area" class="div-design-area">
		<div id="app-wrap" class="div-design-area view-mobile">
		<?php if ($products === false) { ?>
			<div id="view-front" class="labView active">
				<div class="product-design">
					<strong><?php echo lang('designer_product_data_found'); ?></strong>
				</div>
			</div>
		<?php } else { ?>
			
			<!-- begin front design -->						
			<div id="view-front" class="labView active">
				<div class="product-design"></div>
				<div class="design-area"><div class="content-inner"></div></div>
			</div>						
			<!-- end front design -->
			
			<!-- begin back design -->
			<div id="view-back" class="labView">
				<div class="product-design"></div>
				<div class="design-area"><div class="content-inner"></div></div>
			</div>
			<!-- end back design -->
			
			<!-- begin left design -->
			<div id="view-left" class="labView">
				<div class="product-design"></div>
				<div class="design-area"><div class="content-inner"></div></div>
			</div>
			<!-- end left design -->
			
			<!-- begin right design -->
			<div id="view-right" class="labView">
				<div class="product-design"></div>
				<div class="design-area"><div class="content-inner"></div></div>
			</div>
			<!-- end right design -->
			
		<?php } ?>
		</div>
	</div>
	
	<!-- BEGIN help functions -->
	<?php if(cssShow($settings, 'show_toolbar') == ''){ ?>
		<div id="dg-help-functions">
			<div class="btn-group btn-group-sm" role="group">
				<span class="btn btn-default" onclick="design.tools.flip('x')">
					<i class="glyph-icon flaticon-reflect flaticon-12"></i>
				</span>					
				<span class="btn btn-default" onclick="design.tools.move('vertical')">
					<i class="glyph-icon flaticon-center-alignment-1 flaticon-12"></i>
				</span>
				<span class="btn btn-default" onclick="design.tools.move('horizontal')">
					<i class="glyph-icon flaticon-squares-1 flaticon-12"></i>
				</span>	
				<span class="btn btn-default" onclick="design.tools.move('left')">
					<i class="glyph-icon flaticon-back-1 flaticon-12"></i>
				</span>	
				<span class="btn btn-default" onclick="design.tools.move('right')">
					<i class="glyph-icon flaticon-next-1 flaticon-12"></i>
				</span>	
				<span class="btn btn-default" onclick="design.tools.move('up')">
					<i class="glyph-icon flaticon-up-arrow flaticon-12"></i>
				</span>	
				<span class="btn btn-default" onclick="design.tools.move('down')">
					<i class="glyph-icon flaticon-down-arrow flaticon-12"></i>
				</span>
				<span class="btn btn-default" onclick="design.tools.remove()">
					<i class="glyph-icon flaticon-interface flaticon-12"></i>
				</span>
				<span class="btn btn-default" onclick="design.tools.reset(this)">
					<i class="glyph-icon flaticon-rotate flaticon-12"></i>
				</span>
				<?php $addons->view('tools'); ?>
			</div>
		</div>
	<?php } ?>
	<!-- END help functions -->
</div>
<div class="in-canvas-price">
<div class="product-prices">
	<div id="product-price" <?php echo cssShow($settings, 'show_total_price'); ?>>
		<span class="product-price-title"><?php echo lang('designer_right_total'); ?></span>
		<div class="product-price-list">
			<span id="product-price-old">
				<?php echo $settings->currency_symbol; ?><span class="price-old-number"></span>
			</span>
			<span id="product-price-sale">
				<?php echo $settings->currency_symbol; ?><span class="price-sale-number"></span>
			</span>
		</div>
		<span class="price-restart" title="<?php echo lang('designer_get_price'); ?>" onclick="design.ajax.getPrice()"><i class="glyphicons restart"></i></span>
	</div>
	<?php $addons->view('cart'); ?>
	<!--<button <?php //echo cssShow($settings, 'show_add_to_cart', 1); ?> type="button" class="btn btn-warning btn-addcart" onclick="design.ajax.addJs(this)"><i class="glyph-icon flaticon-shopping-cart-3"></i> <span><?php //echo "In den Warenkorb";//echo lang('designer_right_buy_now'); ?></span></button>-->				
</div>
</div>
<div class="product-views">
	<div class="" id="product-thumbs"></div>	
</div>