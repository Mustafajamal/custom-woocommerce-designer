<?php
$settings = $GLOBALS['settings'];
$addons = $GLOBALS['addons'];
?>
<div class="col-left">
	<div id="dg-left" class="width-100">
		<div class="dg-box width-100">
			<ul class="menu-left">		
				<li class="edit_product_element">
					<!--<a class="btn_edit_product" href="javascript:void(0);" onclick="e_display('.col-right', 'show')">
						<i class="glyph-icon flaticon-fashion sm-flaticon"></i> <span><?php //echo lang('edit_product'); ?></span>
					</a>-->
					<a <?php echo cssShow($settings, 'show_product'); ?> href="#dg-products" class="view_change_products btn btn-none" data-toggle="modal" data-target="#dg-products">
						<!--<i class="glyph-icon flaticon-menu"></i>-->
						<!--<svg class="svg-inline--fa fa-futbol fa-w-16" aria-hidden="true" focusable="false" data-prefix="far" data-icon="futbol" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 496 512" data-fa-i2svg=""><path fill="currentColor" d="M483.8 179.4C449.8 74.6 352.6 8 248.1 8c-25.4 0-51.2 3.9-76.7 12.2C41.2 62.5-30.1 202.4 12.2 332.6 46.2 437.4 143.4 504 247.9 504c25.4 0 51.2-3.9 76.7-12.2 130.2-42.3 201.5-182.2 159.2-312.4zm-74.5 193.7l-52.2 6.4-43.7-60.9 24.4-75.2 71.1-22.1 38.9 36.4c-.2 30.7-7.4 61.1-21.7 89.2-4.7 9.3-10.7 17.8-16.8 26.2zm0-235.4l-10.4 53.1-70.7 22-64.2-46.5V92.5l47.4-26.2c39.2 13 73.4 38 97.9 71.4zM184.9 66.4L232 92.5v73.8l-64.2 46.5-70.6-22-10.1-52.5c24.3-33.4 57.9-58.6 97.8-71.9zM139 379.5L85.9 373c-14.4-20.1-37.3-59.6-37.8-115.3l39-36.4 71.1 22.2 24.3 74.3-43.5 61.7zm48.2 67l-22.4-48.1 43.6-61.7H287l44.3 61.7-22.4 48.1c-6.2 1.8-57.6 20.4-121.7 0z"></path></svg>-->
						<svg class="svg-inline--fa fa-futbol fa-w-16" aria-hidden="true" focusable="false" data-prefix="far" data-icon="pencil-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
						<path d="M508.691,102.645c-4.69-6.632-12.967-9.211-20.591-6.418l-125.493,45.947c-31.312-30.686-88.626-78.495-143.921-81.112
									c-27.261-1.287-54.876,6.956-84.395,25.211c-28.489,17.618-54.426,42.297-71.458,59.328
									c-31.041,31.042-51.568,70.202-59.359,113.244c-7.611,42.048-2.662,84.908,14.313,123.946c1.549,3.564,5.531,5.388,9.242,4.236
									c12.252-3.805,25.612-0.952,35.266,7.074l-21.249,16.833c-3.177,2.517-3.826,7.08-1.476,10.382
									c6.963,9.788,14.79,19.048,23.263,27.521c1.472,1.472,3.422,2.23,5.388,2.23c1.474,0,2.957-0.427,4.252-1.301l6.683-4.505
									c56.764-38.254,109.994-82.285,158.212-130.87c12.751-12.848,25.17-26.053,37.257-39.57l51.126-18.24
									c0.082-0.029,0.165-0.061,0.247-0.093c70.605-27.992,133.5-73.731,181.883-132.272
									C513.06,117.952,513.385,109.283,508.691,102.645z M155.532,368.902c-6.927-6.98-13.551-14.036-19.682-21.002
									c-2.779-3.156-7.59-3.463-10.746-0.684c-3.156,2.779-3.463,7.59-0.685,10.746c6.034,6.854,12.526,13.785,19.301,20.648
									c-23.467,18.985-47.828,37.01-73.074,54.025l-1.397,0.941c-4.568-4.864-8.908-9.974-12.976-15.276l22.069-17.483
									c1.584-1.254,2.602-3.085,2.836-5.091c0.232-2.006-0.341-4.022-1.595-5.605c-11.988-15.131-31.511-22.406-50.266-19.256
									c-13.791-34.674-17.55-72.325-10.856-109.306c5.296-29.255,16.934-56.577,34.096-80.432c-0.74,6.078-1.15,12.917-0.981,20.476
									c1.232,54.914,31.844,105.185,57.309,137.688c1.502,1.917,3.74,2.919,5.999,2.919c1.643,0,3.298-0.529,4.69-1.62
									c3.311-2.593,3.891-7.38,1.298-10.69c-23.806-30.385-52.507-77.173-54.043-127.517c-0.908-29.705,8.244-46.961,8.436-47.315
									c0.112-0.202,0.209-0.407,0.302-0.614c0.202-0.088,0.403-0.183,0.6-0.29c0.089-0.049,9.099-4.904,25.221-7.243
									c14.898-2.161,39.114-2.554,69.868,7.846c0.808,0.274,1.63,0.403,2.44,0.403c3.175,0,6.138-2.002,7.212-5.177
									c1.347-3.984-0.79-8.305-4.774-9.653c-32.173-10.88-58.205-11.189-76.585-8.569c12.817-11.373,27.453-23.084,42.751-32.544
									c26.818-16.583,51.572-24.091,75.666-22.951c51.059,2.416,107.126,50.695,132.894,75.719c-16.263,24.22-33.454,47.637-51.54,70.23
									c-31.19-30.724-62.687-54.124-93.669-69.538c-3.768-1.873-8.337-0.339-10.209,3.426c-1.873,3.766-0.339,8.336,3.425,10.209
									c29.88,14.864,60.418,37.67,90.798,67.767C249.188,283.14,204.382,328.167,155.532,368.902z M496.143,114.516
									c-46.726,56.535-107.458,100.715-175.635,127.768l-25.561,9.119c25.305-30.038,48.943-61.501,70.68-94.118l127.71-46.757
									c1.495-0.548,2.414,0.194,2.922,0.912C496.769,112.161,497.164,113.281,496.143,114.516z"/></path></svg>
						<span>Produkte</span>
					</a>
				</li>
				<?php $addons->view('menu-left', array(), 'mobile'); ?>
				<li <?php echo cssShow($settings, 'show_add_art'); ?> class="add_clipart_element">
					<a href="#dg-cliparts" class="add_item_clipart" title="" data-toggle="modal" data-target="#dg-cliparts">
						<!--<i class="glyph-icon flaticon-photo sm-flaticon"></i> <span><?php //echo lang('designer_menu_add_art'); ?></span>-->
						<svg class="svg-inline--fa fa-pencil-alt fa-w-16" aria-hidden="true" focusable="false" data-prefix="far" data-icon="pencil-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M491.609 73.625l-53.861-53.839c-26.378-26.379-69.075-26.383-95.46-.001L24.91 335.089.329 484.085c-2.675 16.215 11.368 30.261 27.587 27.587l148.995-24.582 315.326-317.378c26.33-26.331 26.581-68.879-.628-96.087zM200.443 311.557C204.739 315.853 210.37 318 216 318s11.261-2.147 15.557-6.443l119.029-119.03 28.569 28.569L210 391.355V350h-48v-48h-41.356l170.259-169.155 28.569 28.569-119.03 119.029c-8.589 8.592-8.589 22.522.001 31.114zM82.132 458.132l-28.263-28.263 12.14-73.587L84.409 338H126v48h48v41.59l-18.282 18.401-73.586 12.141zm378.985-319.533l-.051.051-.051.051-48.03 48.344-88.03-88.03 48.344-48.03.05-.05.05-.05c9.147-9.146 23.978-9.259 33.236-.001l53.854 53.854c9.878 9.877 9.939 24.549.628 33.861z"></path></svg>
						<span>Designs</span>
					</a>
				</li>
				<li <?php echo cssShow($settings, 'show_add_text'); ?> class="add_text_element">
					<a href="javascript:void(0);" class="add_item_text" title="">
						<!--<i class="glyph-icon flaticon-type sm-flaticon"></i> <span><?php //echo lang('designer_menu_add_text'); ?></span>-->
						<svg class="svg-inline--fa fa-text-height fa-w-18" aria-hidden="true" focusable="false" data-prefix="far" data-icon="text-height" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M560 368h-56V144h56c14.31 0 21.33-17.31 11.31-27.31l-80-80a16 16 0 0 0-22.62 0l-80 80C379.36 126 384.36 144 400 144h56v224h-56c-14.31 0-21.32 17.31-11.31 27.31l80 80a16 16 0 0 0 22.62 0l80-80C580.64 386 575.64 368 560 368zM304 32H16A16 16 0 0 0 0 48v80a16 16 0 0 0 16 16h16a16 16 0 0 0 16-16V96h80v336H80a16 16 0 0 0-16 16v16a16 16 0 0 0 16 16h160a16 16 0 0 0 16-16v-16a16 16 0 0 0-16-16h-48V96h80v32a16 16 0 0 0 16 16h16a16 16 0 0 0 16-16V48a16 16 0 0 0-16-16z"></path></svg>
						<span>Text</span>
					</a>
				</li>							
				<li <?php echo cssShow($settings, 'show_add_upload'); ?> class="upload_image_element">
					<a href="#dg-myclipart" title="" data-toggle="modal" data-target="#dg-myclipart">
						<!--<i class="glyph-icon flaticon-upload flaticon-22"></i> <span><?php //echo lang('designer_help_upload'); ?></span>-->
						<svg class="svg-inline--fa fa-image fa-w-16" aria-hidden="true" focusable="false" data-prefix="far" data-icon="image" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M464 64H48C21.49 64 0 85.49 0 112v288c0 26.51 21.49 48 48 48h416c26.51 0 48-21.49 48-48V112c0-26.51-21.49-48-48-48zm-6 336H54a6 6 0 0 1-6-6V118a6 6 0 0 1 6-6h404a6 6 0 0 1 6 6v276a6 6 0 0 1-6 6zM128 152c-22.091 0-40 17.909-40 40s17.909 40 40 40 40-17.909 40-40-17.909-40-40-40zM96 352h320v-80l-87.515-87.515c-4.686-4.686-12.284-4.686-16.971 0L192 304l-39.515-39.515c-4.686-4.686-12.284-4.686-16.971 0L96 304v48z"></path></svg>
						<span>HOCHLADEN</span>
					</a>
				</li>
				
				<li class="buy_now_element">
					<a href="#" onclick="e_display('.col-right', 'show')" class="btn_add_cart">
						<!--<i class="glyph-icon flaticon-shopping-cart-3 flaticon-22"></i>--> 
						<svg class="svg-inline--fa fa-shopping-cart fa-w-18" aria-hidden="true" focusable="false" data-prefix="fal" data-icon="shopping-cart" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M551.991 64H129.28l-8.329-44.423C118.822 8.226 108.911 0 97.362 0H12C5.373 0 0 5.373 0 12v8c0 6.627 5.373 12 12 12h78.72l69.927 372.946C150.305 416.314 144 431.42 144 448c0 35.346 28.654 64 64 64s64-28.654 64-64a63.681 63.681 0 0 0-8.583-32h145.167a63.681 63.681 0 0 0-8.583 32c0 35.346 28.654 64 64 64 35.346 0 64-28.654 64-64 0-17.993-7.435-34.24-19.388-45.868C506.022 391.891 496.76 384 485.328 384H189.28l-12-64h331.381c11.368 0 21.177-7.976 23.496-19.105l43.331-208C578.592 77.991 567.215 64 551.991 64zM240 448c0 17.645-14.355 32-32 32s-32-14.355-32-32 14.355-32 32-32 32 14.355 32 32zm224 32c-17.645 0-32-14.355-32-32s14.355-32 32-32 32 14.355 32 32-14.355 32-32 32zm38.156-192H171.28l-36-192h406.876l-40 192z"></path></svg>
						<span><?php echo "Preis"; ?></span>
					</a>
				</li>
				
				<!--<li <?php //echo cssShow($settings, 'show_layers'); ?> class="item_layers_element">
					<a href="#" onclick="e_display('.view-modal-layers', 'show')">
						<i class="glyph-icon flaticon-layers-1 flaticon-22"></i> <span><?php //echo lang('designer_menu_login_layers'); ?></span>
					</a>
				</li>
				<li <?php //echo cssShow($settings, 'show_add_team'); ?> class="team_number_element">
					<a href="javascript:void(0);" class="add_item_team" title="">
						<i class="glyph-icon flaticon-football"></i> <span><?php //echo lang('designer_teams'); ?></span>
					</a>
				</li>
				<li <?php //echo cssShow($settings, 'show_my_design'); ?> class="mydesign_element">
					<a href="javascript:void(0);" class="add_item_mydesign" <?php //echo cssShow($settings, 'show_my_design'); ?>>
						<i class="glyph-icon flaticon-user flaticon-22"></i> <span><?php //echo lang('designer_menu_my_design'); ?></span>
					</a>
				</li>-->				
				<!--<li <?php //echo cssShow($settings, 'show_add_qrcode'); ?> class="add_QRcode_element">
					<a href="javascript:void(0);" class="add_item_qrcode" title="">
						<i class="glyph-icon flaticon-qr-code flaticon-22"></i> <span><?php //echo lang('designer_menu_add_qrcode'); ?></span>
					</a>
				</li>-->
			</ul>
		</div>			
	</div>
</div>